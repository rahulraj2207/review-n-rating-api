import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:review/controllers/readController.dart';
import 'package:review/utils/colors/colors.dart';
import 'package:get/get.dart';
import 'package:review/controllers/listController.dart';
class ReviewRead extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ReadController controller = Get.put(ReadController());




    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.grey,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Reviews",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        elevation: 0,
      ),
      body: Container(
        color: Colors.white,
        child: Obx (
                  (){
               if (controller.readData.isEmpty)
                 return Center(child: CircularProgressIndicator());
                 else
                 return ListView.builder(
              itemCount: controller.readData.length,
              itemBuilder: (context, i) {

                DateTime dateTimeCreatedAt = DateTime.parse(controller.readData[i].createdAt.toString());
                DateTime dateTimeNow = DateTime.now();
                final differenceInDays = dateTimeNow.difference(dateTimeCreatedAt).inDays;


                return ReviewTile(
                reviewText: controller.readData[i].review==null?"hi":controller.readData[i].review,
                  userName: controller.readData[i].reviewerUsername,
                  imgPath: controller.readData[i].reviewerDp,
                  rating: controller.readData[i].rating.toDouble(),
                  reviewCount: controller.readData[i].numberOfReviews,
                  days: differenceInDays,
                );
              });

  }
      ),
    ));
  }
}

class ReviewTile extends StatelessWidget {
  final String reviewText;
  final String userName;
  final String imgPath;
  final double rating;
  final int reviewCount;
  final int days;
  ReviewTile(
      {this.reviewText,
      this.imgPath,
      this.userName,
      this.rating,
      this.reviewCount,
      this.days});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            radius: 25.0,
            backgroundImage: NetworkImage(imgPath),
            backgroundColor: Colors.transparent,
          ),
          SizedBox(width: 15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 5,
              ),
              Text(
                userName,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Text(
                reviewCount == 1
                    ? "$reviewCount review"
                    : "$reviewCount reviews",
                style: TextStyle(color: Colors.grey),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: RatingBarIndicator(
                        rating: rating,
                        itemCount: 5,
                        itemSize: 15.0,
                        unratedColor: Colors.grey[300],
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: kPrimaryColor,
                        ),
                      )),
                  SizedBox(
                    width: 15,
                  ),
                  Text(
                    days == 1 ? "$days day ago" : "$days days ago",
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.7,
                child: Text(
                  reviewText,
                  softWrap: true,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

// class ReviewContent {
//   final String reviewText;
//   final String userName;
//   final String imgPath;
//   final double rating;
//   final int reviewCount;
//   final int days;
//
//   ReviewContent(
//       {this.reviewText,
//       this.imgPath,
//       this.userName,
//       this.rating,
//       this.reviewCount,
//       this.days});
// }
//
// List<ReviewContent> contents = [
//   ReviewContent(
//     reviewText: "Test review",
//     userName: 'dunder.mifflin',
//     imgPath: 'https://via.placeholder.com/150',
//     rating: 4,
//     reviewCount: 1,
//     days: 12,
//   ),
//   ReviewContent(
//     reviewText:
//         "Lorem ipsem dolor sit amet, more test nn new comment , more test comment, more test comment, more test comment.",
//     userName: 'sarah.connor',
//     imgPath: 'https://via.placeholder.com/150',
//     rating: 5,
//     reviewCount: 3,
//     days: 14,
//   )
// ];
