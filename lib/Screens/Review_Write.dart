import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter/material.dart';
import 'package:review/utils/colors/colors.dart';
import 'package:get/get.dart';
import 'package:review/controllers/writeController.dart';

final TextEditingController _controller = TextEditingController();


final WriteController controller = Get.put(WriteController());
double rating;


final snackBar = SnackBar(
  content: Text('Thank you for sharing'),
  action: SnackBarAction(
    label: 'Edit',
    onPressed: () {
      Get.to(ReviewWrite());
      // Some code to undo the change.
    },
  ),
);

class ReviewWrite extends StatefulWidget {



  void clearText() {
    _controller.clear();
  }


  @override
  _ReviewWriteState createState() => _ReviewWriteState();
}

class _ReviewWriteState extends State<ReviewWrite> {
  final _formKey = GlobalKey<FormState>();




  final WriteController controller = Get.put(WriteController());



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.grey,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Review",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        elevation: 0,
      ),
      body: Form(
        key: _formKey,

        child: Center(
          child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            SizedBox(
              height: 30,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: Image.network(
                'https://via.placeholder.com/150',
                height: 80.0,
                width: 80.0,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text("Michael Scott",
                style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold)),
            SizedBox(
              height: 5,
            ),
            Text("Leave your review", style: TextStyle(color: Colors.grey)),
            SizedBox(
              height: 30,
            ),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: RatingBar.builder(
                  onRatingUpdate: (rating1) {
                    setState(() {
                       rating= rating1;
                       return rating;
                    });


                  },
                  initialRating: 1,
                  itemCount: 5,
                  itemSize: 30.0,
                  unratedColor: Colors.grey[300],
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: kPrimaryColor,
                  ),
                )),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child:
                  Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(
                  "Review",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.all(15.0),
                  child: SingleChildScrollView(
                    child: TextFormField(
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'Text is empty';
                        }
                        return null;
                      },

                      controller: _controller,
                      onChanged: (val) {

                      },
                      maxLength: 400,
                      maxLines: 8,
                      style: TextStyle(fontWeight: FontWeight.bold),
                      decoration: InputDecoration.collapsed(


                          hintText: "Write something..",
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey[100],
                  ),
                ),
              ]),
            ),





            FlatButton(
              minWidth: MediaQuery.of(context).size.width * 0.9,
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
              onPressed: () {

                if (_formKey.currentState.validate()) {
                  WriteController().editReviewApi(rating,_controller.text);
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }

              },
              child: Text('submit', style: TextStyle(fontSize: 16)),
              color: _controller.text==null ? Colors.grey : kPrimaryColor,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            )


          ]),
        ),
      ),
    );
  }
}
