import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review/Screens/Review_Write.dart';
import 'package:review/Screens/Review_Read.dart';
import 'package:review/Screens/screen.dart';
class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Center(
            child: Text(
              'Task routes',
              style: TextStyle(color: Colors.black),
            )),
      ),
      body: Center(
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 50),


                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 150, height: 50),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.deepOrange),
                      ),
                      child: Text('Review Write'),
                      onPressed: () {
                        Get.to(ReviewWrite());
                      },
                    ),
                  ),
                ),

                SizedBox(height: 50),


                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 150, height: 50),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.deepOrange),
                      ),
                      child: Text('Review Read'),
                      onPressed: () {
                        Get.to(ReviewRead());
                      },
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 150, height: 50),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.deepOrange),
                      ),
                      child: Text('api'),
                      onPressed: () {
                        Get.to(screen());
                      },
                    ),
                  ),
                ),




              ],
            )
          ],
        ),
      ),
    );
  }
}
