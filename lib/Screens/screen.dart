import 'package:flutter/material.dart';
import 'package:review/controllers/viewController.dart';
import 'package:get/get.dart';
import 'package:review/controllers/viewedStoryController.dart';
class screen extends StatefulWidget {
  @override
  _screenState createState() => _screenState();
}

class _screenState extends State<screen> {
  ViewController viewController= Get.put(ViewController());
  ViewedStoryController viewedStoryController = Get.put(ViewedStoryController());
  @override
  void initState() {
    viewController.viewsFetchApi();
    viewedStoryController.viewedStory();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
