// To parse this JSON data, do
//
//     final storyData = storyDataFromJson(jsonString);

import 'dart:convert';

StoryData storyDataFromJson(String str) => StoryData.fromJson(json.decode(str));

String storyDataToJson(StoryData data) => json.encode(data.toJson());

class StoryData {
  StoryData({
    this.appData,
  });

  String appData;

  factory StoryData.fromJson(Map<String, dynamic> json) => StoryData(
    appData: json["app_data"],
  );

  Map<String, dynamic> toJson() => {
    "app_data": appData,
  };
}
