// To parse this JSON data, do
//
//     final storyList = storyListFromJson(jsonString);

import 'dart:convert';

List<StoryList> storyListFromJson(String str) => List<StoryList>.from(json.decode(str).map((x) => StoryList.fromJson(x)));

String storyListToJson(List<StoryList> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class StoryList {
  StoryList({
    this.id,
    this.profile,
    this.story,
    this.description,
    this.event,
    this.createdAt,
    this.views,
    this.viewsCount,
  });

  int id;
  Profile profile;
  List<Story> story;
  String description;
  Event event;
  DateTime createdAt;
  List<View> views;
  int viewsCount;

  factory StoryList.fromJson(Map<String, dynamic> json) => StoryList(
    id: json["id"],
    profile: Profile.fromJson(json["profile"]),
    story: List<Story>.from(json["story"].map((x) => Story.fromJson(x))),
    description: json["description"],
    event: Event.fromJson(json["event"]),
    createdAt: DateTime.parse(json["created_at"]),
    views: List<View>.from(json["views"].map((x) => View.fromJson(x))),
    viewsCount: json["views_count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "profile": profile.toJson(),
    "story": List<dynamic>.from(story.map((x) => x.toJson())),
    "description": description,
    "event": event.toJson(),
    "created_at": createdAt.toIso8601String(),
    "views": List<dynamic>.from(views.map((x) => x.toJson())),
    "views_count": viewsCount,
  };
}

class Event {
  Event({
    this.id,
    this.name,
  });

  int id;
  String name;

  factory Event.fromJson(Map<String, dynamic> json) => Event(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}

class Profile {
  Profile({
    this.id,
    this.dp,
    this.firstname,
    this.surname,
    this.username,
  });

  int id;
  String dp;
  String firstname;
  String surname;
  String username;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
    id: json["id"],
    dp: json["dp"],
    firstname: json["firstname"],
    surname: json["surname"],
    username: json["username"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "dp": dp,
    "firstname": firstname,
    "surname": surname,
    "username": username,
  };
}

class Story {
  Story({
    this.id,
    this.storyFile,
  });

  int id;
  String storyFile;

  factory Story.fromJson(Map<String, dynamic> json) => Story(
    id: json["id"],
    storyFile: json["story_file"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "story_file": storyFile,
  };
}

class View {
  View({
    this.profileId,
    this.firstname,
    this.surname,
    this.username,
    this.dp,
    this.isFollowing,
  });

  int profileId;
  String firstname;
  String surname;
  String username;
  String dp;
  bool isFollowing;

  factory View.fromJson(Map<String, dynamic> json) => View(
    profileId: json["profile_id"],
    firstname: json["firstname"],
    surname: json["surname"],
    username: json["username"],
    dp: json["dp"],
    isFollowing: json["is_following"],
  );

  Map<String, dynamic> toJson() => {
    "profile_id": profileId,
    "firstname": firstname,
    "surname": surname,
    "username": username,
    "dp": dp,
    "is_following": isFollowing,
  };
}
