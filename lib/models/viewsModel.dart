// To parse this JSON data, do
//
//     final viewsModel = viewsModelFromJson(jsonString);

import 'dart:convert';

List<ViewsModel> viewsModelFromJson(String str) => List<ViewsModel>.from(json.decode(str).map((x) => ViewsModel.fromJson(x)));

String viewsModelToJson(List<ViewsModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ViewsModel {
  ViewsModel({
    this.profileId,
    this.firstname,
    this.surname,
    this.username,
    this.dp,
    this.isFollowing,
  });

  int profileId;
  String firstname;
  String surname;
  String username;
  String dp;
  bool isFollowing;

  factory ViewsModel.fromJson(Map<String, dynamic> json) => ViewsModel(
    profileId: json["profile_id"],
    firstname: json["firstname"],
    surname: json["surname"],
    username: json["username"],
    dp: json["dp"],
    isFollowing: json["is_following"],
  );

  Map<String, dynamic> toJson() => {
    "profile_id": profileId,
    "firstname": firstname,
    "surname": surname,
    "username": username,
    "dp": dp,
    "is_following": isFollowing,
  };
}
