// To parse this JSON data, do
//
//     final readData = readDataFromJson(jsonString);

import 'dart:convert';

List<ReadData> readDataFromJson(String str) => List<ReadData>.from(json.decode(str).map((x) => ReadData.fromJson(x)));

String readDataToJson(List<ReadData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ReadData {
  ReadData({
    this.id,
    this.reviewerId,
    this.reviewerFirstname,
    this.reviewerSurname,
    this.reviewerUsername,
    this.reviewerDp,
    this.rating,
    this.review,
    this.createdAt,
    this.numberOfReviews,
  });

  int id;
  int reviewerId;
  String reviewerFirstname;
  String reviewerSurname;
  String reviewerUsername;
  String reviewerDp;
  int rating;
  String review;
  DateTime createdAt;
  int numberOfReviews;

  factory ReadData.fromJson(Map<String, dynamic> json) => ReadData(
    id: json["id"],
    reviewerId: json["reviewer_id"],
    reviewerFirstname: json["reviewer_firstname"],
    reviewerSurname: json["reviewer_surname"],
    reviewerUsername: json["reviewer_username"],
    reviewerDp: json["reviewer_dp"],
    rating: json["rating"],
    review: json["review"],
    createdAt: DateTime.parse(json["created_at"]),
    numberOfReviews: json["number_of_reviews"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "reviewer_id": reviewerId,
    "reviewer_firstname": reviewerFirstname,
    "reviewer_surname": reviewerSurname,
    "reviewer_username": reviewerUsername,
    "reviewer_dp": reviewerDp,
    "rating": rating,
    "review": review,
    "created_at": createdAt.toIso8601String(),
    "number_of_reviews": numberOfReviews,
  };
}
