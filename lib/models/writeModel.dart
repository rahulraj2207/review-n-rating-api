// To parse this JSON data, do
//
//     final reviewData = reviewDataFromJson(jsonString);

import 'dart:convert';

ReviewData reviewDataFromJson(String str) => ReviewData.fromJson(json.decode(str));

String reviewDataToJson(ReviewData data) => json.encode(data.toJson());

class ReviewData {
  ReviewData({
    this.event,
    this.rating,
    this.review,
  });

  int event;
  int rating;
  String review;

  factory ReviewData.fromJson(Map<String, dynamic> json) => ReviewData(
    event: json["event"],
    rating: json["rating"],
    review: json["review"],
  );

  Map<String, dynamic> toJson() => {
    "event": event,
    "rating": rating,
    "review": review,
  };
}
