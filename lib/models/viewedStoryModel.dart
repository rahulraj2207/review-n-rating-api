// To parse this JSON data, do
//
//     final viewsModel = viewsModelFromJson(jsonString);

import 'dart:convert';

ViewsModel viewsModelFromJson(String str) => ViewsModel.fromJson(json.decode(str));

String viewsModelToJson(ViewsModel data) => json.encode(data.toJson());

class ViewsModel {
  ViewsModel({
    this.story,
  });

  int story;

  factory ViewsModel.fromJson(Map<String, dynamic> json) => ViewsModel(
    story: json["story"],
  );

  Map<String, dynamic> toJson() => {
    "story": story,
  };
}
