import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:get/state_manager.dart';
import 'package:get/get.dart';
import 'package:review/services/viewedStoryApi.dart';



class ViewedStoryController extends GetxController {
  final ViewedStoryApi viewedStoryApi = ViewedStoryApi();

  void viewedStory() {
    int storyId = 11;
    viewedStoryApi
        .editStoryViews(
      storyId: storyId,
    )
        .then((response) {
      if (response.statusCode == 200) {
        print(response.body);
      } else {
        print("Failed to save your data");
      }
    }
    );
  }
}







