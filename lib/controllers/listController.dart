import 'dart:convert';
import 'package:get/state_manager.dart';
import 'package:review/models/listModel.dart';
import 'package:review/services/listApi.dart';
import 'package:get/get.dart';
var data ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjIwMTEwMTA2LCJqdGkiOiI5YTI5YWE2OTBiYjA0YTRlOTc5MTIzOTc5ODdiOTQzNCIsInVzZXJfaWQiOjR9.SfEuQq2k7qCRbLRV2ANNmTOSlhKtqjW-kUwc7H7gqME";
class ListController extends GetxController {
  RxBool isLoading = true.obs;
  RxList<StoryList> storyList = <StoryList>[].obs;

  @override
  void onInit() {
    listFetchApi();
    super.onInit();
  }
  void listFetchApi() async {
    try {
      isLoading(true);
      await ListApi().fetchListApi(token: data).then((response) {
        if (response.statusCode == 200) {
          print('listscccss');

          var events2 = storyListFromJson(response.body);
          storyList.addAll(events2);
        } else {
          print('error occured in events details Page');
        }
      });
    }finally{
      isLoading(false);
    }
  }
}

