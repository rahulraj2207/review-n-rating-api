import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:review/services/writeApi.dart';
import 'package:get/get.dart';
import 'package:review/Screens/Review_Write.dart';
import 'package:bot_toast/bot_toast.dart';

var data = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE5OTMxNDcxLCJqdGkiOiI0YzQzYjIyMjRhMDI0OWEyOTI2ZTJlNGI4NzFkNDU5ZSIsInVzZXJfaWQiOjR9.GlKNLiDykfW63mtc9uv3rCbUBu3c_DTY6Aya3Ko2Pfs";


class WriteController extends GetxController {
  final WriteApi profileEdit = WriteApi();

    void editReviewApi(double rating,String review) {
      int event = 31;
      profileEdit
          .editReview(
        event: event,
        rating: rating,
        review: review.toString(),
        token: data,
      )
          .then((response) {
        if (response.statusCode == 200) {
          ReviewWrite().clearText();
          print(response.body);
        } else {
          print("Failed to save your data");
        }
      }
      );
    }
}







