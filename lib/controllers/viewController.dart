import 'package:get/state_manager.dart';
import 'package:review/services/viewsApi.dart';
import 'package:get/get.dart';
import 'package:review/models/viewsModel.dart';
var data ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjIwMTk4ODQ3LCJqdGkiOiJlNWEzN2NmYjMwOGQ0NzBlYjA0MjYyZmFhY2M3MmJiZCIsInVzZXJfaWQiOjR9.2Pvsx3vjNjtSpX8dr7AYwEMEfRiKKlZZD2oM2N2phRo";

class ViewController extends GetxController {
  RxBool isLoading = true.obs;
  RxList<ViewsModel> viewsData = <ViewsModel>[].obs;

  @override
  void onInit() {
    // viewsFetchApi();
    super.onInit();
  }
  int viewer=11;
  void viewsFetchApi() async {
    try {
      isLoading(true);
      await ViewsApi().fetchViewsApi(token: data,viewer: viewer).then((response) {
        if (response.statusCode == 200) {
          var events2 = viewsModelFromJson(response.body);
          print('view controller $events2');
        } else {
          print('error occured in events details Page');
        }
      });
    }finally{
      isLoading(false);
    }
  }
}

