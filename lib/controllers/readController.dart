import 'dart:convert';
import 'package:get/state_manager.dart';
import 'package:review/models/readModel.dart';
import 'package:review/services/readApi.dart';
import 'package:get/get.dart';
var data ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE5OTMxNDcxLCJqdGkiOiI0YzQzYjIyMjRhMDI0OWEyOTI2ZTJlNGI4NzFkNDU5ZSIsInVzZXJfaWQiOjR9.GlKNLiDykfW63mtc9uv3rCbUBu3c_DTY6Aya3Ko2Pfs";
class ReadController extends GetxController {
  RxBool isLoading = true.obs;
  RxList<ReadData> readData = <ReadData>[].obs;

  @override
  void onInit() {
    todaysEventsFetchApi();
    super.onInit();
  }
  void todaysEventsFetchApi() async {
    try {
      isLoading(true);
      await ReadApi().fetchTodaysApi(token: data).then((response) {
        if (response.statusCode == 200) {
          var events2 = readDataFromJson(response.body);
          readData.addAll(events2);
        } else {
          print('error occured in events details Page');
        }
      });
    }finally{
      isLoading(false);
    }
  }
}

