import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:review/models/viewedStoryModel.dart';
var data = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjIwMTk4ODQ3LCJqdGkiOiJlNWEzN2NmYjMwOGQ0NzBlYjA0MjYyZmFhY2M3MmJiZCIsInVzZXJfaWQiOjR9.2Pvsx3vjNjtSpX8dr7AYwEMEfRiKKlZZD2oM2N2phRo";

class ViewedStoryApi {

  Future<http.Response> editStoryViews({
    int storyId,
  }) async {
    final client = http.Client();
    http.Response response;
    try {
      CircularProgressIndicator();
      response = await client.post(Uri.parse(
          'https://51c046c6-73be-447a-a38c-d999464b1b85.mutualevents.co/api/v1/story/view/'),
          body: json.encode({
            'story': storyId,

          }),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $data',
          });
      return response;
    } catch (error) {
      print('this is my error  api');
      print(error.toString());
      throw 'throw response check / api';
    }
  }
}











