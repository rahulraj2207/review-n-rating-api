import 'package:http/http.dart' as http;

class ViewsApi{
  Future<http.Response> fetchViewsApi({String token,int viewer}) async {
    final client = http.Client();
    http.Response response;
    try {
      response = await client.get(Uri.parse("https://51c046c6-73be-447a-a38c-d999464b1b85.mutualevents.co/api/v1/story/views/$viewer/1/10/"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      print(response.body);
      print(response.statusCode);
      return response;
    } catch (error) {
      print('this is my error');
      print(error.toString());
      throw 'throw response check';
    }
  }
}