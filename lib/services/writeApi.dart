import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:review/controllers/readController.dart';
import 'package:review/models/writeModel.dart';
var data = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjIwMTk4ODQ3LCJqdGkiOiJlNWEzN2NmYjMwOGQ0NzBlYjA0MjYyZmFhY2M3MmJiZCIsInVzZXJfaWQiOjR9.2Pvsx3vjNjtSpX8dr7AYwEMEfRiKKlZZD2oM2N2phRo";

class WriteApi {

  Future<http.Response> editReview({
    String token,
    int event,
    double rating,
    String review,
  }) async {
    final client = http.Client();
    http.Response response;
    try {
      CircularProgressIndicator();
      response = await client.post(Uri.parse(
          'https://51c046c6-73be-447a-a38c-d999464b1b85.mutualevents.co/api/v1/event/review/write/'),
          body: json.encode({
            'event': event,
            'rating': rating,
            'review': review,
          }),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $token',
          });
      return response;
    } catch (error) {
      print('this is my error in profile edit api');
      print(error.toString());
      throw 'throw response check profile edit api';
    }
  }
}











